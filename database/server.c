#include <stdio.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <pthread.h>
#include <stdio.h>
#include <dirent.h>
#include <stdlib.h>
#include <sys/types.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>

#define PORT 8080

int setup_sock();
void run_exec(char path[], char *argv[]);
void split_string(char *str1, char delimiter);

int open_record(char *fileName);
void close_record(int fileName);
void create_user(char *namaAkun, char *passAkun);
void use_db(char *DBName);
void grant_permission(char *nama_DB, char *nama_user);
void open_folder(char *folderName);
void create_db(char *namaFolder, char *namaUser);
void create_table(char *namaTabel, char *kolom1);
void drop_DB(char *namaDB);
void drop_table(char *namaTabel);
void drop_column(char *namaTabel, char *namaKolom);

void insert_data(char *namaTabel, char *data);
void update(char *namaTabel, char *namaKolom, char *value);
void delete(char *namaTabel);
void select_all(char *namaTabel);

int server_fd,
    new_socket;
struct sockaddr_in address;
int opt = 1;
char buffer[1024] = {0};
int addrlen = sizeof(address);
char splittedString[50][100] = {0};
int splitTotal = 0;

char cwd[256];
char saya[256];
char DB_dipakai[256] = "DB_akun";
char *root_dir = "/home/ryohilmi/projects/fp-sisop-b12-2022/database";

int main(int argc, char const *argv[])
{
    getcwd(cwd, sizeof(cwd));

    if (setup_sock() < 0)
        return -1;

    while (true)
    {
        memset(buffer, 0, sizeof(buffer));
        read(new_socket, buffer, 1024);

        split_string(buffer, ':');

        if (strstr(buffer, "create_user") != NULL)
        {
            create_user(splittedString[1], splittedString[2]);
        }
        else if (strstr(buffer, "grant_permission") != NULL)
        {
            grant_permission(splittedString[1], splittedString[2]);
        }
        else if (strstr(buffer, "use") != NULL)
        {
            use_db(splittedString[1]);
        }
        else if (strstr(buffer, "create_db") != NULL)
        {
            create_db(splittedString[1], splittedString[2]);
        }
        else if (strstr(buffer, "create_table") != NULL)
        {
            char columns[1024] = {0};
            strcpy(columns, splittedString[3]);

            for (int i = 4; i < splitTotal; i++)
            {
                strcat(columns, " ");
                strcat(columns, splittedString[i]);
            }

            create_table(splittedString[1], columns);
        }
        else if (strstr(buffer, "drop") != NULL)
        {
            if (strcmp(splittedString[1], "db") == 0)
            {
                drop_DB(splittedString[2]);
            }
            else if (strcmp(splittedString[1], "table") == 0)
            {
                drop_table(splittedString[2]);
            }
            else if (strcmp(splittedString[1], "column") == 0)
            {
                drop_column(splittedString[2], splittedString[3]);
            }
        }
        else if (strstr(buffer, "insert_into") != NULL)
        {
            char columns[1024] = {0};
            strcpy(columns, splittedString[3]);

            for (int i = 4; i < splitTotal; i++)
            {
                strcat(columns, " ");
                strcat(columns, splittedString[i]);
            }

            insert_data(splittedString[1], columns);
        }
        else if (strstr(buffer, "update") != NULL)
        {
            update(splittedString[1], splittedString[2], splittedString[3]);
        }
        else if (strstr(buffer, "delete") != NULL)
        {
            delete(splittedString[1]);
        }
        else if (strstr(buffer, "select_all") != NULL)
        {
            select_all(splittedString[1]);
        }
    }
}

int setup_sock()
{
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)))
    {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0)
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0)
    {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0)
    {
        perror("accept");
        exit(EXIT_FAILURE);
    }
}

void run_exec(char path[], char *argv[])
{
    pid_t child_id;
    int status;

    child_id = fork();

    if (child_id < 0)
    {
        exit(EXIT_FAILURE);
    }

    if (child_id == 0)
    {
        execv(path, argv);
        exit(0);
    }
    else
    {
        while ((wait(&status)) > 0)
            ;
    }
}

//  OPEN RECORD, TABEL. hak akses
int open_record(char *fileName)
{
    int fd;

    fd = open("list_akun.txt", O_CREAT | O_RDWR | O_APPEND, 0644);
    if (fd == -1)
    {
        perror("open_record");
    }
    else
        printf("data akses terbuka\n");

    return fd;
}

//  CLOSE RECORD, TABEL. hak akses
void close_record(int fileName)
{
    close(fileName);
}

// CREATE USER, menambah data ke database akun
// CREATE USER [nama_user] IDENTIFIED BY [pass_user]
void create_user(char *namaAkun, char *passAkun)
{
    int i;
    char target[256];

    strcpy(target, cwd);
    strcat(target, "/databases/DB_akun/list_akun.txt");

    char *arg[] = {"mkdir", "-p", "databases/DB_akun", NULL};
    run_exec("/bin/mkdir", arg);

    FILE *fptr = fopen(target, "a");
    if (fptr == NULL)
    {
        printf("Could not open");
    }
    fprintf(fptr, "\n%s %s", namaAkun, passAkun);

    fclose(fptr);
}

// USE DATABASE
//   USE [nama_DB]
//   kayak chdir
void use_db(char *DBName)
{
    chdir(root_dir);

    strcpy(DB_dipakai, DBName);

    chdir("databases");

    if (chdir(DBName) != 0)
        perror("chdir() failed\n");
    else
        printf("Current database: %s\n", DBName);
}

// GRANT PERMISSION
//   GRANT PERMISSION [nama_DB] INTO [nama_user]
void grant_permission(char *nama_DB, char *nama_user)
{
    char target[256];

    chdir(root_dir);

    strcpy(target, cwd);
    strcat(target, "/databases/DB_akun/list_aksesDB.txt");
    FILE *fptr = fopen(target, "a+");
    if (fptr == NULL)
    {
        printf("Could not open");
    }
    fprintf(fptr, "\n%s %s", nama_DB, nama_user);

    fclose(fptr);
}

// OPEN FOLDER
void open_folder(char *folderName)
{
    DIR *dir;
    struct dirent *sd;

    dir = opendir(folderName);

    if (dir == NULL)
    {
        printf("Error! Unable to open directory.\n");
        exit(1);
    }
    else
        printf("folder %s telah terbuka\n", folderName);

    while ((sd = readdir(dir)) != NULL)
    {
        printf(">> %s\n", sd->d_name);
    }

    closedir(dir);
}

// CREATE DATABASE
//   CREATE DATABASE [nama_db]
// membuat folder
void create_db(char *namaFolder, char *namaUser)
{
    char target[256];

    chdir(root_dir);

    strcpy(target, cwd);
    strcat(target, "/databases/");
    strcat(target, namaFolder);

    printf("%s\n", target);

    char *arg[] = {"mkdir", "-p", "databases/DB_akun", NULL};
    run_exec("/bin/mkdir", arg);

    grant_permission(namaFolder, namaUser);

    // if (mkdir(target, 0777))
    // {
    //     printf("Directory created\n");
    // }
    // else
    // {
    //     printf("Unable\n");
    //     exit(1);
    // }
}

// CREATE TABLE
//   CREATE TABLE
void create_table(char *namaTabel, char *kolom1)
{
    // membuat file
    char target[256];

    chdir(root_dir);

    strcpy(target, cwd);
    strcat(target, "/databases/");
    strcat(target, DB_dipakai);
    strcat(target, "/");
    strcat(target, namaTabel);
    strcat(target, ".txt");

    FILE *fp = NULL;
    fp = fopen(target, "a");

    if (fp == NULL)
    {
        perror("open_record");
    }
    else
        printf("tabel telah terbuat\n");

    fprintf(fp, "%s\n", kolom1);

    fclose(fp);
}

// DROP DB
// DROP DATABASE [nama_DB]
void drop_DB(char *namaDB)
{
    char target[256];

    chdir(root_dir);

    strcpy(target, cwd);
    strcat(target, "/databases/");
    strcat(target, namaDB);

    char *arg[] = {"rm", "-rf", target, NULL};
    run_exec("/bin/rm", arg);
}

// DROP TABLE
// DROP TABLE [nama_table];
void drop_table(char *namaTabel)
{
    char target[256];

    chdir(root_dir);

    strcpy(target, cwd);
    strcat(target, "/databases/");
    strcat(target, DB_dipakai);
    strcat(target, "/");
    strcat(target, namaTabel);
    strcat(target, ".txt");

    char *arg[] = {"rm", target, NULL};
    run_exec("/bin/rm", arg);
}

// DROP COLUMN
// DROP COLUMN [nama_kolom] FROM [nama_tabel];
void drop_column(char *namaTabel, char *namaKolom)
{
    char target[256];
    char temp[256];

    chdir(root_dir);

    strcpy(target, cwd);
    strcat(target, "/databases/");
    strcat(target, DB_dipakai);
    strcat(target, "/");
    strcpy(temp, target);
    strcat(temp, "temp.txt");
    strcat(target, namaTabel);
    strcat(target, ".txt");

    FILE *ftemp = fopen(temp, "w+");

    // menghitung banyak kolom
    int triedCreating = 0;
    FILE *filePtr = fopen(target, "r+");

    char kata[256];
    char kalimat[100];
    int i, index;

    fgets(kalimat, 100, filePtr);

    int wordCount = 0;
    for (int i = 0; i < strlen(kalimat) - 1; i++)
    {
        if (kalimat[i] == ' ' && isalpha(kalimat[i + 1]) && (i > 0))
        {
            wordCount++;
        }
    }
    wordCount++;

    fclose(filePtr);

    FILE *file = fopen(target, "r+");

    for (i = 0; i < wordCount; i++)
    {
        fscanf(file, "%s", kata);
        if (!strcmp(kata, namaKolom))
            index = i;
        else
            fprintf(ftemp, "%s ", kata);
    }
    int c, j = 0, flag;

    while (1)
    {
        flag = fscanf(file, "%s", kata);

        if (flag != 1)
            break;

        // printf("kata: %s\t", kata);
        // printf("\nkata: %s\tj: %d\twordCount: %d\tnilai: %d\n", kata, j, wordCount, j % wordCount);

        // BUKAN INDEKS YANG DIHAPUS
        if ((j % wordCount) != index)
        {
            fprintf(ftemp, "%s ", kata);
        }
        j++;
    }

    fclose(file);
    fclose(ftemp);

    FILE *lastTemp = fopen(temp, "r+");
    FILE *lastStep = fopen(target, "w+");

    for (i = 0; i < wordCount - 1; i++)
    {
        fscanf(lastTemp, "%s", kata);
        fprintf(lastStep, "%s ", kata);
    }

    int k = 0;
    while (1)
    {
        flag = fscanf(lastTemp, "%s", kata);

        if (flag != 1)
            break;

        if ((k % (wordCount - 1)) == 0)
        {
            fprintf(lastStep, "\n");
        }
        // COPY PASTE KE FILE ASLI
        fprintf(lastStep, "%s ", kata);

        // printf("\nkata: %s\tk: %d\twordCount: %d\tnilai: %d\n", kata, k, wordCount, k % (wordCount -1));
        k++;
    }

    fprintf(lastStep, "\n");

    fclose(lastStep);
    fclose(lastTemp);

    char *arg[] = {"rm", temp, NULL};
    run_exec("/bin/rm", arg);
}

void split_string(char *str1, char delimiter)
{
    splitTotal = 0;

    memset(splittedString, 0, sizeof(splittedString));
    int i, j = 0, ctr = 0;
    for (i = 0; i <= (strlen(str1)); i++)
    {
        if (str1[i] == delimiter || str1[i] == '\0' || str1[i] == ';')
        {
            splittedString[ctr][j] = '\0';
            ctr++;
            splitTotal++;
            j = 0;
        }
        else
        {
            splittedString[ctr][j] = str1[i];
            j++;
        }
    }
}

// INSERT INTO [nama_tabel] [value]
void insert_data(char *namaTabel, char *data)
{
    char target[256];

    chdir(root_dir);

    strcpy(target, cwd);
    strcat(target, "/databases/");
    strcat(target, DB_dipakai);
    strcat(target, "/");
    strcat(target, namaTabel);
    strcat(target, ".txt");

    FILE *fp = NULL;
    fp = fopen(target, "a");

    fprintf(fp, "%s\n", data);

    fclose(fp);
}

// UPDATE
// UPDATE [nama_table] SET [nama_kolom] = value
void update(char *namaTabel, char *namaKolom, char *value)
{
    char target[256];
    char temp[256];

    chdir(root_dir);

    strcpy(target, cwd);
    strcat(target, "/databases/");
    strcat(target, DB_dipakai);
    strcat(target, "/");
    strcpy(temp, target);
    strcat(temp, "temp.txt");
    strcat(target, namaTabel);
    strcat(target, ".txt");

    FILE *ftemp = fopen(temp, "w+");

    // menghitung banyak kolom
    int triedCreating = 0;
    FILE *filePtr = fopen(target, "r+");

    char kata[256];
    char kalimat[100];
    int i, index;

    fgets(kalimat, 100, filePtr);
    // printf("kalimat: %s\n", kalimat);

    int wordCount = 0;
    for (int i = 0; i < strlen(kalimat) - 1; i++)
    {
        if (kalimat[i] == ' ' && isalpha(kalimat[i + 1]) && (i > 0))
        {
            wordCount++;
        }
    }
    wordCount++;

    fclose(filePtr);

    FILE *file = fopen(target, "r+");

    for (i = 0; i < wordCount; i++)
    {
        fscanf(file, "%s", kata);
        if (!strcmp(kata, namaKolom))
            index = i;

        fprintf(ftemp, "%s ", kata);
    }
    int c, j = 0, flag;

    while (1)
    {
        flag = fscanf(file, "%s", kata);

        if (flag != 1)
            break;

        // printf("kata: %s\t", kata);
        // printf("\nkata: %s\tj: %d\twordCount: %d\tnilai: %d\n", kata, j, wordCount, j % wordCount);

        // BUKAN YANG DIUPDATE
        if ((j % wordCount) != index)
        {
            fprintf(ftemp, "%s ", kata);
        }
        else
        {
            fprintf(ftemp, "%s ", value);
        }
        j++;
    }

    fclose(file);
    fclose(ftemp);

    FILE *lastTemp = fopen(temp, "r+");
    FILE *lastStep = fopen(target, "w+");

    for (i = 0; i < wordCount; i++)
    {
        fscanf(lastTemp, "%s", kata);
        fprintf(lastStep, "%s ", kata);
    }

    int k = 0;
    while (1)
    {
        flag = fscanf(lastTemp, "%s", kata);

        if (flag != 1)
            break;

        if ((k % wordCount) == 0)
        {
            fprintf(lastStep, "\n");
        }
        // COPY PASTE KE FILE ASLI
        fprintf(lastStep, "%s ", kata);

        // printf("\nkata: %s\tk: %d\twordCount: %d\tnilai: %d\n", kata, k, wordCount, k % (wordCount -1));
        k++;
    }

    fclose(lastStep);
    fclose(lastTemp);

    char *arg[] = {"rm", temp, NULL};
    run_exec("/bin/rm", arg);
}

// DELETE
// DELETE FROM [nama_tabel]
void delete(char *namaTabel)
{
    char target[256];
    char kalimat[256] = {0};

    chdir(root_dir);

    strcpy(target, cwd);
    strcat(target, "/databases/");
    strcat(target, DB_dipakai);
    strcat(target, "/");
    strcat(target, namaTabel);
    strcat(target, ".txt");

    FILE *filePtr = fopen(target, "r+");
    if (filePtr == NULL)
    {
        printf("File tidak ditemukan\n");
        return;
    }
    fgets(kalimat, 256, filePtr);
    fclose(filePtr);

    FILE *fileOpen = fopen(target, "w+");
    fprintf(fileOpen, "%s", kalimat);
    fclose(fileOpen);
}

void select_all(char *namaTabel)
{
    char target[256];
    char result[4096] = {0};
    char c;

    strcpy(target, cwd);
    strcat(target, "/databases/");
    strcat(target, DB_dipakai);
    strcat(target, "/");
    strcat(target, namaTabel);
    strcat(target, ".txt");

    char row[100] = {0};

    FILE *fptr = fopen(target, "r");
    while (fgets(row, 100, fptr) != NULL)
    {
        strcat(result, row);
    }
    fclose(fptr);

    send(new_socket, result, strlen(result), 0);
}
