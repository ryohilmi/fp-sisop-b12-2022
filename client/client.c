#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <stdbool.h>
#include <pthread.h>
#include <ctype.h>

#define PORT 8080
#define HOST "127.0.0.1"

int setup_sock();
struct sockaddr_in address;
int sock = 0, valread;
struct sockaddr_in serv_addr;
char buffer[1024];

int opt;

char username[50] = {0};
char password[50] = {0};
bool isRoot = false;
char splittedString[20][100] = {0};
int splitTotal = 0;

void check_user(int argc, char const *argv[]);
void split_string(char *str1);

int main(int argc, char const *argv[])
{
    check_user(argc, argv);

    if (setup_sock() < 0)
        return -1;

    while (true)
    {
        // send(sock, "HALOHALO", strlen("HALOHALO"), 0);
        // printf("Hello message sent\n");
        // valread = read(sock, buffer, 1024);
        // printf("%s\n", buffer);
        char command[1024] = {0};
        fgets(command, 1024, stdin);

        command[strlen(command) - 1] = '\0';
        split_string(command);

        if (strstr(command, "CREATE USER") != NULL)
        {
            char data[1024] = {0};

            strcpy(data, "create_user");
            strcat(data, ":");
            strcat(data, splittedString[2]);
            strcat(data, ":");
            strcat(data, splittedString[5]);

            send(sock, data, strlen(data), 0);
        }
        else if (strstr(command, "GRANT PERMISSION") != NULL)
        {
            char data[1024] = {0};

            strcpy(data, "grant_permission");
            strcat(data, ":");
            strcat(data, splittedString[2]);
            strcat(data, ":");
            strcat(data, splittedString[4]);

            send(sock, data, strlen(data), 0);
        }
        else if (strstr(command, "USE") != NULL)
        {
            char data[1024] = {0};

            strcpy(data, "use");
            strcat(data, ":");
            strcat(data, splittedString[1]);

            send(sock, data, strlen(data), 0);
        }
        else if (strstr(command, "CREATE DATABASE") != NULL)
        {
            char data[1024] = {0};

            strcpy(data, "create_db");
            strcat(data, ":");
            strcat(data, splittedString[2]);
            strcat(data, ":");
            strcat(data, username);

            send(sock, data, strlen(data), 0);
        }
        else if (strstr(command, "CREATE TABLE") != NULL)
        {
            char data[1024] = {0};
            int totalColumn = (splitTotal - 4) / 2;

            int length = snprintf(NULL, 0, "%d", totalColumn);
            char *str = malloc(length + 1);
            snprintf(str, length + 1, "%d", totalColumn);

            strcpy(data, "create_table");
            strcat(data, ":");
            strcat(data, splittedString[2]);
            strcat(data, ":");
            strcat(data, str);

            for (int i = 0; i < totalColumn * 2; i += 2)
            {
                strcat(data, ":");
                strcat(data, splittedString[i + 3]);
            }

            send(sock, data, strlen(data), 0);
        }
        else if (strstr(command, "DROP DATABASE") != NULL)
        {
            char data[1024] = {0};

            strcpy(data, "drop:db");
            strcat(data, ":");
            strcat(data, splittedString[2]);

            send(sock, data, strlen(data), 0);
        }
        else if (strstr(command, "DROP TABLE") != NULL)
        {
            char data[1024] = {0};

            strcpy(data, "drop:table");
            strcat(data, ":");
            strcat(data, splittedString[2]);

            send(sock, data, strlen(data), 0);
        }
        else if (strstr(command, "DROP COLUMN") != NULL)
        {
            char data[1024] = {0};

            strcpy(data, "drop:column");
            strcat(data, ":");
            strcat(data, splittedString[2]);
            strcat(data, ":");
            strcat(data, splittedString[3]);

            send(sock, data, strlen(data), 0);
        }
        else if (strstr(command, "INSERT INTO") != NULL)
        {
            char data[1024] = {0};
            int totalColumn = (splitTotal - 3);

            int length = snprintf(NULL, 0, "%d", totalColumn);
            char *str = malloc(length + 1);
            snprintf(str, length + 1, "%d", totalColumn);

            strcpy(data, "insert_into");
            strcat(data, ":");
            strcat(data, splittedString[2]);
            strcat(data, ":");
            strcat(data, str);

            for (int i = 0; i < totalColumn; i++)
            {
                strcat(data, ":");
                strcat(data, splittedString[i + 3]);
            }

            send(sock, data, strlen(data), 0);
        }
        else if (strstr(command, "UPDATE") != NULL)
        {
            char data[1024] = {0};

            strcpy(data, "update");
            strcat(data, ":");
            strcat(data, splittedString[1]);
            strcat(data, ":");
            strcat(data, splittedString[3]);
            strcat(data, ":");
            strcat(data, splittedString[4]);

            send(sock, data, strlen(data), 0);
        }
        else if (strstr(command, "DELETE") != NULL)
        {
            char data[1024] = {0};

            strcpy(data, "delete");
            strcat(data, ":");
            strcat(data, splittedString[2]);

            send(sock, data, strlen(data), 0);
        }
        else if (strstr(command, "SELECT *") != NULL)
        {
            char data[1024] = {0};
            char result[4096] = {0};

            strcpy(data, "select_all");
            strcat(data, ":");
            strcat(data, splittedString[3]);

            send(sock, data, strlen(data), 0);
            read(sock, result, 4096);

            printf("%s", result);
        }
    }

    return 0;
}

int setup_sock()
{
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("\n Socket creation error \n");
        return -1;
    }

    memset(&serv_addr, '0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    if (inet_pton(AF_INET, HOST, &serv_addr.sin_addr) <= 0)
    {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        printf("\nConnection Failed \n");
        return -1;
    }
}

void check_user(int argc, char const *argv[])
{
    if (!geteuid())
        isRoot = true;

    while ((opt = getopt(argc, argv, "u:p:")) != -1)
    {
        switch (opt)
        {
        case 'u':
            strcpy(username, optarg);
            break;
        case 'p':
            strcpy(password, optarg);
            break;
        }
    }
}

void split_string(char *str1)
{
    splitTotal = 0;

    memset(splittedString, 0, sizeof(splittedString));
    int i, j = 0, ctr = 0;
    for (i = 0; i <= (strlen(str1)); i++)
    {
        if (str1[i] == ' ' || str1[i] == '\0' || str1[i] == ';')
        {
            splittedString[ctr][j] = '\0';
            ctr++;
            splitTotal++;
            j = 0;
        }
        else
        {
            splittedString[ctr][j] = str1[i];
            j++;
        }
    }
}
